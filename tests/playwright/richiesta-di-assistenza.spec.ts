import { test, expect, defineConfig } from '@playwright/test';


export default defineConfig({
  use: {
    screenshot: 'only-on-failure',
  },
});

test('Richiesta d\'assistenza', async ({ page }) => {
 
  const EMAIL = 'test.automatico@e3e.test';

  await page.goto('https://www.comune-qa.bugliano.pi.it/');
  const locator = page.frameLocator('body')

  await page.screenshot({ path: 'screenshot.png', fullPage: false });
  await page.getByRole('link', { name: 'Richiedi assistenza' }).click();
  
  await page.getByLabel('Nome*', { exact: true }).click();
  await page.getByLabel('Nome*', { exact: true }).fill('Marco');
  
  await page.getByLabel('Cognome*').click();
  await page.getByLabel('Cognome*').fill('Belcognome');
  
  await page.getByLabel('Email*').click();
  await page.getByLabel('Email*').fill(EMAIL);
  
  await page.getByText('Codice Fiscale*').click();
  await page.getByLabel('Codice Fiscale*').fill('CCCSSS81M12H123U');
  
  await page.getByText('Dettagli*').click();
  await page.getByLabel('Dettagli*').fill('Questa è una richiesta utile ad effettuare un test. Non serve ad altro.\n\nGrazie\nSaluti');
  
  await page.getByText('Ho letto e compreso l’').click();
  await page.getByText('Ho letto e compreso l’').click();
  await page.getByRole('button', { name: 'Invia icon' }).click();
  
  await expect(page.getByText('La richiesta di assistenza è stata inviata con successo')).toBeVisible();
  await expect(page.getByText(EMAIL)).toBeVisible();
});
