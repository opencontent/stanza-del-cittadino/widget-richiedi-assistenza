import React from "react";
import { t } from "i18next";

export { Errors };

function Errors({ error }) {
  return (
      <div>
        {error &&
        (error.name === "TypeError" ||
            error.name === "TimeoutError" ||
            error.toString() === "ApolloError: signal timed out" ||
            error.toString() === "ApolloError: Timeout exceeded") ? (
            <div className="d-flex align-items-center justify-content-center vh-100">
              <div>
                <h3>{t("L'operazione sta impiegando più tempo del previsto")}</h3>
                <p>{t("Ricarica la pagina oppure riprova più tardi")}</p>
              </div>
            </div>
        ) : error && error.name === "ErrorBoundary" ? (
            <div className="d-flex align-items-center justify-content-center vh-100">
              <div>
                <h3>{t("Servizio momentaneamente non disponibile!")}</h3>
                <p>
                  {t(
                      "Ci scusiamo per il disagio e vi invitiamo a riprovare in seguito.",
                  )}
                </p>
              </div>
            </div>
        ) : error && error.name === "ApolloError" ? (
            <div className="d-flex align-items-center justify-content-center vh-100">
              <div>
                <h3>{t("Servizio momentaneamente non disponibile!")}</h3>
                <p>
                  {t(
                      "Ci scusiamo per il disagio e vi invitiamo a riprovare in seguito.",
                  )}
                </p>
              </div>
            </div>
        ) : (
            null
        )}
      </div>
  );
}
