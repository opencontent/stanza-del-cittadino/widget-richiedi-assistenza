import { Col, Container, Icon, Row } from 'design-react-kit';
import { t } from 'i18next';
import React from "react";
import { Trans } from 'react-i18next';
import { useSelector } from 'react-redux';
import Breadcrumbs from "../_components/Breadcrumbs/Breadcrumbs";

export { ThankYouPage };

function ThankYouPage() {
  const { application } = useSelector((x) => x.form);

  return (
    <Container>
      <Row className="justify-content-center mb-50">
        <Col className="col-12" lg={10}>
          {window.OC_RENDER_BREADCRUMB === true || process.env.REACT_APP_OC_RENDER_BREADCRUMB === 'true' ?  <Breadcrumbs></Breadcrumbs> : null}
          <div className="cmp-hero">
            <section className={'it-hero-wrapper bg-white align-items-start'}>
              <div className="it-hero-text-wrapper pt-0 ps-0 pb-40 pb-lg-60">
                <div className="categoryicon-top d-flex">
                  <Icon color="success" icon="it-check-circle" className={' mr-10 icon-sm mb-1'} />
                  <h1 className="text-black hero-title">{t('request_sent')}</h1>
                </div>
                <div className="hero-text">
                  <p>{t('thanks_message_report_sent')}</p>
                  <p className="pt-3 pt-lg-4">
                    <Trans t={t} i18nKey={'thanks_message_report_sent_email'}></Trans>
                    <br />
                    <strong>{application?.data.email}</strong>
                  </p>
                </div>
              </div>
            </section>
          </div>
        </Col>
      </Row>
    </Container>
  );
}
