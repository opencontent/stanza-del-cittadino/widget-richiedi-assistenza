import { Navigate } from "react-router-dom";
import { Home } from "./home/Home";
import { ThankYouPage } from "./pages/ThankYouPage";

/*
 path: route path
 name: i18n name
 Component: Element React to render
 */
export const routes =  [
  {
    path: "/richiedi_assistenza",
    name: "report_list",
    Component: <Home></Home>
  },
  {
    path: "/richiedi_assistenza/thankyou",
    name: "request_sent",
    Component: <ThankYouPage></ThankYouPage>
  },
  {
    path: "/*",
    name: "report_list",
    Component:
      <Navigate to="/richiedi_assistenza" />
  },
];
