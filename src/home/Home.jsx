import {
  Accordion,
  AccordionBody,
  AccordionHeader,
  AccordionItem,
  Alert,
  Button,
  Col,
  Container,
  Icon,
  Input,
  Label,
  LinkList,
  NavItem,
  Row,
  TextArea,
  useNavScroll
} from 'design-react-kit';

import React, { useEffect, useLayoutEffect, useRef, useState } from "react";
import { Controller, useForm } from 'react-hook-form';
import { Trans, useTranslation } from 'react-i18next';

import { useSelector, useDispatch } from 'react-redux';

import { Link, useNavigate, useSearchParams } from 'react-router-dom';
import useScrollPercentage from 'react-scroll-percentage-hook';
import Breadcrumbs from "../_components/Breadcrumbs/Breadcrumbs";
import { apiActions, authActions, currentUserActions } from '../_store';
import { storeApplication } from '../_store/form.slice';
import { HashLink } from 'react-router-hash-link';
import CodiceFiscale from 'codice-fiscale-js';

export { Home };

function Home() {
  const dispatch = useDispatch();
  const navigate = useNavigate();

  const { t } = useTranslation();
  const [collapseElementOpen, setCollapseElement] = useState('1');
  const [btsCssVersion, setBtsCssVersion] = useState(null);
  const { application, service_id } = useSelector((x) => x.api);
  const { currentUser } = useSelector((x) => x.currentUser);
  const { ref, percentage } = useScrollPercentage({ windowScroll: true });
  const [showAlert, setShowAlert] = useState(false);
  const { token } = useSelector((x) => x.auth);
  const [searchParams] = useSearchParams();
  const details = searchParams.get('details');
  let validateEmailRegex = /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;

  /**NavScroll**/
  const containerRef = useRef(null);
  const { register: registerScroll, isActive } = useNavScroll({
    root: containerRef.current || undefined
  });
  const getActiveClass = (id) => (isActive(id) ? 'active' : undefined);
  /** **/

  // Form management
  const {
    handleSubmit,
    formState: { errors, isDirty, isValid, isSubmitting },
    control,
    reset,
    getValues,
    setValue
  } = useForm({
    mode: 'onChange',
    shouldUnregister: true,
    defaultValues: {
      name: '',
      surname: '',
      email: '',
      details: '',
      fiscal_code: '',
      privacy: false
    }
  });

  // recovery token otherwise I create it
  useEffect(() => {
    dispatch(authActions.getSessionToken()).then((el) => {
      if (el.meta.requestStatus === 'rejected') {
        dispatch(authActions.createSessionToken())
          .unwrap()
          .then(async (response) => {
          if (response?.id) {
            dispatch(currentUserActions.getUserById({id: response?.id}));
          }
        });
      } else {
        if(el.payload?.id) {
          dispatch(currentUserActions.getUserById({id: el.payload?.id}))
          .unwrap().
            then(() => {
              dispatch(apiActions.getDraftApplication());
            });
        }
      }
    });
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [dispatch]);

  // If auth user get data user
  useEffect(() => {
    if (token && currentUser?.nome !== 'Anonymous') {
      setValue('name', currentUser.nome, {
        shouldValidate: false,
        shouldDirty: true
      });
      setValue('surname', currentUser.cognome, {
        shouldValidate: false,
        shouldDirty: true
      });
      setValue('email', currentUser.email, {
        shouldValidate: false,
        shouldDirty: true
      });
      setValue('fiscal_code', currentUser.codice_fiscale, {
        shouldValidate: true,
        shouldDirty: true
      });
    }
  }, [dispatch, setValue, currentUser, token]);

  // Get categories and service helpdesk
  useEffect(() => {
    if (!service_id?.id) {
      dispatch(apiActions.getServiceHelpDesk());
    }
  }, [dispatch, service_id?.id]);


  useLayoutEffect(() =>{
    if(document.readyState === 'complete'){
      let version = parseFloat(getComputedStyle(document.documentElement).getPropertyValue('--bootstrap-italia-version').replace('"',''))
      setBtsCssVersion(version)
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  },[document.readyState])

  // get draft application
  useEffect(() => {
    if (application?.data) {
      if (application?.data?.hasOwnProperty('applicant')) {
        setValue('name', application?.data?.applicant?.data?.completename?.data.name, {
          shouldValidate: true,
          shouldDirty: true
        });
        setValue('surname', application?.data?.applicant?.data?.completename?.data.surname, {
          shouldValidate: true,
          shouldDirty: true
        });
        setValue('fiscal_code', application?.data?.applicant?.data?.fiscal_code?.data.fiscal_code, {
          shouldValidate: true,
          shouldDirty: true
        });
      }
      if (application?.data?.hasOwnProperty('email')) {
        setValue('email', application?.data?.email, {
          shouldValidate: true,
          shouldDirty: true
        });
      }
      if (application?.data?.hasOwnProperty('details')) {
        setValue('details', application?.data?.details, {
          shouldValidate: true,
          shouldDirty: true
        });
      } else if (details) {
        setValue('details', details, {
          shouldValidate: true,
          shouldDirty: true
        });
      }

      if (application?.data?.hasOwnProperty('privacy')) {
        setValue('privacy', application?.data?.privacy, {
          shouldValidate: true,
          shouldDirty: true
        });
      }
    } else if (details) {
      setValue('details', details, {
        shouldValidate: true,
        shouldDirty: true
      });
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [setValue, application, details]);



  const _reset = () => {
    reset();
    dispatch(apiActions.restoreApiApplication());
  }

  const onPrev = () => {
    navigate(-1);
  };

  const isTrue = (value) => value || t('required_field');

  const onSubmit = (data) => {
    if (isSubmitting) {
      return;
    }
    const getId =
      application?.id || application?.data?.length
        ? application?.id || application?.data[0]?.id
        : null;
    let dataApplication = {
      ...(getId && { id: getId }),
      service: service_id?.identifier,
      data: {
        applicant: {
          data: {
            completename: {
              data: {
                name: getValues().name,
                surname: getValues().surname
              }
            },
            fiscal_code: {
              data: {
                fiscal_code: getValues().fiscal_code
              }
            }
          }
        },
        details: getValues().details,
        privacy: getValues().privacy,
        email: getValues().email
      },
      status: data?.status ? '1000' : '1900'
    };

    dispatch(storeApplication(dataApplication));

    if (dataApplication.status === '1900') {
      if (application?.id) {
        return dispatch(
          apiActions.updateApplication({ data: dataApplication, id: dataApplication.id })
        ).then((response) => {
          if (response?.payload) {
            _reset();
            navigate(`/richiedi_assistenza/thankyou`, {replace: true});
          }
        });
      } else {
        return dispatch(apiActions.createApplication(dataApplication)).then((response) => {
          if (response?.payload) {
            _reset();
            dispatch(storeApplication(response?.payload));
            navigate(`/richiedi_assistenza/thankyou`, {replace: true});
          }
        });
      }
    } else if (dataApplication.status === '1000') {
      if (dataApplication?.id) {
        return dispatch(
          apiActions.updateApplication({ data: dataApplication, id: dataApplication.id })
        ).then(() => {
          setShowAlert(true);
          setTimeout(() => {
            setShowAlert(false);
          }, 4000);
        });
      } else {
        return dispatch(apiActions.createApplication(dataApplication)).then((response) => {
          if (response?.payload) {
            dispatch(storeApplication(response?.payload));
          }
          setShowAlert(true);
          setTimeout(() => {
            setShowAlert(false);
          }, 4000);
        });
      }
    }
  };

  const saveApplication = () => {
    onSubmit({ status: '1000' });
  };

/*   const validateEmail = (email) => {
    return validateEmailRegex.test(email);
  }; */


  const checkIsValidCF = (val) =>{

    if(CodiceFiscale.check(val)){
      return true
    }else{
      return t('fiscal_code_validator');
    }
  }

  return (
    <form ref={ref} onSubmit={handleSubmit(onSubmit)}>
      <Container>
        <Row className="justify-content-center">
          <Col className="col-12" lg={10}>
            {window.OC_RENDER_BREADCRUMB === true || process.env.REACT_APP_OC_RENDER_BREADCRUMB === 'true' ?  <Breadcrumbs></Breadcrumbs> : null}
          </Col>
        </Row>
      </Container>
      <Container id="intro" tag="section">
        <Row className="justify-content-center">
          <Col className="col-12" lg={10}>
            <h1>{t('report_list')}</h1>
            <p className="subtitle-small">
              {t('report_list_description')}
              <br />
              <br />
              {currentUser?.nome === 'Anonymous' && (
                <Trans
                  t={t}
                  values={{
                    url: window.OC_AUTH_URL + '?return-url=' + window.location.href
                  }}
                  i18nKey={'auth_description'}
                  components={{ Link: <Link /> }}
                ></Trans>
              )}
            </p>
          </Col>
        </Row>
      </Container>
      <Container>
        <Row className={'mt-lg-50'}>
          <Col lg={3} className={'d-lg-block mb-4 d-none'}>
            <div className="cmp-navscroll sticky-top" aria-labelledby="accordion-title-one">
              <nav className="navbar it-navscroll-wrapper navbar-expand-lg" aria-label={t('info')}>
                <div className="navbar-custom" id="navbarNavProgress">
                  <div className="menu-wrapper">
                    <div className="link-list-wrapper">
                      <Accordion>
                        <AccordionItem>
                          <AccordionHeader
                            id="accordion-title-one"
                            className="pb-10 px-3"
                            active={collapseElementOpen === '1'}
                            onToggle={() =>
                              setCollapseElement(collapseElementOpen !== '1' ? '1' : '')
                            }
                          >
                            {' '}
                            {t('info')}{' '}
                            {btsCssVersion < 2.7 ? (<Icon icon={'it-expand'} size={'xs'} className={'right'}></Icon>) : null}
                          </AccordionHeader>{' '}
                          <div className="progress">
                            <div
                              className="progress-bar it-navscroll-progressbar"
                              role="progressbar"
                              aria-valuenow={percentage.vertical}
                              aria-valuemin="0"
                              aria-valuemax="100"
                              style={{
                                width: percentage.vertical + '%'
                              }}
                            ></div>
                          </div>
                          <AccordionBody active={collapseElementOpen === '1'}>
                            <div
                              id="collapse-one"
                              className="accordion-collapse collapse show"
                              role="region"
                              aria-labelledby="accordion-title-one"
                            >
                              <div className="accordion-body">
                                <LinkList data-element="page-index">
                                  <NavItem>
                                    <HashLink
                                      smooth
                                      className={getActiveClass('applicant')}
                                      to="#applicant"
                                    >
                                      <span className="title-medium">{t('applicant')}</span>
                                    </HashLink>
                                  </NavItem>
                                  <NavItem>
                                    <HashLink
                                      smooth
                                      className={getActiveClass('request')}
                                      to="#request"
                                    >
                                      <span className="title-medium">{t('request')}</span>
                                    </HashLink>
                                  </NavItem>
                                </LinkList>
                              </div>
                            </div>
                          </AccordionBody>
                        </AccordionItem>
                      </Accordion>
                    </div>
                  </div>
                </div>
              </nav>
            </div>
          </Col>
          <Col lg={8} className={'offset-lg-1'}>
            <div className="steppers-content" aria-live="polite">
              <div className="it-page-sections-container">
                <section
                  className="it-page-section"
                  id="applicant"
                  {...registerScroll('applicant')}
                >
                  <div className="cmp-card mb-40">
                    <div className="card has-bkg-grey shadow-sm p-big">
                      <div className="card-header border-0 p-0 mb-lg-30 m-0">
                        <div className="d-flex">
                          <h2 className="title-xxlarge mb-3">{t('applicant')}</h2>
                        </div>
                      </div>
                      <div className="card-body p-0">
                        <div className="form-wrapper bg-white p-4">
                          <Controller
                            name="name"
                            control={control}
                            rules={{ required: {
                                value: true,
                                message: t('required_field')
                              } }}
                            render={({ field }) => (
                              <>
                                <Input
                                  type="text"
                                  id={'name'}
                                  className={'cmp-input mb-0 mt-4'}
                                  label={t('name') + '*'}
                                  placeholder={''}
                                  invalid={errors.name?.type === 'required' ? true
                                    : false}
                                  validationText={
                                    errors.name?.message || t('name_description')
                                  }
                                  innerRef={field.ref}
                                  value={field.value}
                                  onChange={field.onChange}
                                  readOnly={currentUser?.nome !== 'Anonymous'}
                                />
                              </>
                            )}
                          />

                          <Controller
                            name="surname"
                            control={control}
                            rules={{ required: {
                                value: true,
                                message: t('required_field')
                              } }}
                            render={({ field }) => (
                              <>
                                <Input
                                  type="text"
                                  id={'surname'}
                                  className={'cmp-input mb-0'}
                                  label={t('surname') + '*'}
                                  placeholder={''}
                                  invalid={errors.surname?.type === 'required'  ? true
                                    : false}
                                  validationText={
                                    errors.surname?.message || t('surname_description')
                                  }
                                  innerRef={field.ref}
                                  value={field.value}
                                  onChange={field.onChange}
                                  readOnly={currentUser?.nome !== 'Anonymous'}
                                />
                              </>
                            )}
                          />

                          <Controller
                            name="email"
                            control={control}
                            rules={{
                              required: {
                                value: true,
                                message: t('required_field')
                              },
                              pattern: {
                                value: validateEmailRegex,
                                message: t('email_validator')
                              }
                            }}
                            render={({ field }) => (
                              <>
                                <Input
                                  id={'email'}
                                  type="email"
                                  className={'cmp-input mb-0'}
                                  label={t('email') + '*'}
                                  placeholder={''}
                                  invalid={errors.email?.message ? true
                                    : false}
                                  validationText={
                                    errors.email?.message || t('email_description')
                                  }
                                  innerRef={field.ref}
                                  value={field.value}
                                  onChange={field.onChange}
                                />
                              </>
                            )}
                          />

                          <Controller
                            name="fiscal_code"
                            control={control}
                            rules={{
                              required: {
                                value: true,
                                message: t('required_field')
                              },
                              validate: checkIsValidCF, // Usa la funzione custom
                            }}
                            render={({ field }) => (
                              <>
                                <Input
                                  type="text"
                                  id={'fiscal_code'}
                                  className={'cmp-input mb-0'}
                                  label={t('fiscal_code') + '*'}
                                  placeholder={''}
                                  invalid={errors.fiscal_code?.type === 'required' ||  errors.fiscal_code?.type === 'validate' ? true
                                    : false}
                                  innerRef={field.ref}
                                  validationText={
                                    errors.fiscal_code?.message || t('fiscal_code_description')
                                  }
                                  value={field.value}
                                  onChange={(event) => {
                                    setValue('fiscal_code', event.target.value.toUpperCase());
                                    field.onChange(event);
                                  }}
                                  readOnly={currentUser?.nome !== 'Anonymous'}
                                />
                              </>
                            )}
                          />
                        </div>
                      </div>
                    </div>
                  </div>
                </section>

                <section className="it-page-section" id="request" {...registerScroll('request')}>
                  <div className="cmp-card mb-40">
                    <div className="card has-bkg-grey shadow-sm p-big">
                      <div className="card-header border-0 p-0 mb-lg-30 m-0">
                        <div className="d-flex">
                          <h2 className="title-xxlarge mb-3">{t('request')}</h2>
                        </div>
                      </div>
                      <div className="card-body p-0">
                        <div className="form-wrapper bg-white p-4">
                          <div>
                            <Controller
                              name="details"
                              control={control}
                              rules={{
                                required: {
                                  value: true,
                                  message: t('required_field')
                                },
                                maxLength: {
                                  value: 600,
                                  message: t('enter_max_600_char')
                                }
                              }}
                              render={({ field }) => (
                                <div className="cmp-text-area p-0 mt-40">
                                  <TextArea
                                    id={'description'}
                                    label={t('details') + '*'}
                                    className={'text-area'}
                                    rows={2}
                                    placeholder={''}
                                    invalid={
                                      errors.details?.type === 'required' ||
                                      errors.details?.type === 'maxLength'
                                        ? true
                                        : false
                                    }
                                    validationText={
                                      errors.details?.message || t('enter_max_600_char')
                                    }
                                    innerRef={field.ref}
                                    value={field.value}
                                    onChange={field.onChange}
                                  />
                                </div>
                              )}
                            />
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                </section>

                <div className="privacy-wrapper">
                  <p className="text-paragraph mb-3">
                    <Trans
                      t={t}
                      values={{
                        url: window.OC_PRIVACY_URL
                      }}
                      i18nKey={'privacy_description'}
                      components={{ Link: <Link /> }}
                    ></Trans>
                  </p>
                  <div className="form-check mb-2">
                    <div className="checkbox-body d-flex align-items-center flex-wrap">
                      <Controller
                        name="privacy"
                        control={control}
                        rules={{ required: false, validate: isTrue }}
                        render={({ field }) => (
                          <>
                            <Input
                              id="privacy"
                              invalid={errors.privacy?.type === 'required'}
                              innerRef={field.ref}
                              value={field.value}
                              onChange={field.onChange}
                              type="checkbox"
                              aria-label={t('privacy_label')}
                            />
                            <Label
                              for={'privacy'}
                              className="title-small-semi-bold pt-1 mb-0 active"
                              id="privacyDescription"
                            >
                              {t('privacy_label')}
                            </Label>
                          </>
                        )}
                      />
                    </div>
                  </div>
                </div>
              </div>
              <div className="cmp-nav-steps">
                <nav className="steppers-nav" aria-label="Step">
                  <Button
                    type="button"
                    size={'sm'}
                    className="steppers-btn-prev p-0"
                    onClick={onPrev}
                    disabled
                  >
                    <Icon icon={'it-chevron-left'} size={'sm'} color={'primary'}></Icon>
                    <span className="text-button-sm">{t('back')}</span>
                  </Button>
                  {currentUser?.nome !== 'Anonymous' && (
                    <Button
                      type="button"
                      outline
                      color="primary"
                      size={'sm'}
                      className="bg-white steppers-btn-save saveBtn"
                      data-focus-mouse="false"
                      onClick={() => saveApplication()}
                    >
                      <span className="text-button-sm t-primary">{t('save_request')}</span>
                    </Button>
                  )}
                  <Button
                    color="primary"
                    size={'sm'}
                    className="steppers-btn-confirm"
                    disabled={!isValid || !isDirty}
                    type="submit"
                  >
                    <span className="text-button-sm">{t('send')}</span>
                    <Icon
                      icon={'it-chevron-right'}
                      size={'sm'}
                      color={'primary'}
                      className={'icon-white'}
                    ></Icon>
                  </Button>
                </nav>
                {application?.error ? (
                  <Alert color={'danger'} className="cmp-disclaimer rounded">
                    <span className="d-inline-block text-uppercase cmp-disclaimer__message">
                      {t('not_authorized')}
                    </span>
                  </Alert>
                ) : (
                  ''
                )}
                {showAlert ? (
                  <Alert color={'success'} className="cmp-disclaimer rounded">
                    <span className="d-inline-block text-uppercase cmp-disclaimer__message">
                      {t('request_saved_successfully')}
                    </span>
                  </Alert>
                ) : (
                  ''
                )}
              </div>
            </div>
          </Col>
        </Row>
      </Container>
    </form>
  );
}
