import { useEffect, useLayoutEffect } from 'react';
import { Routes, Route, useLocation } from 'react-router-dom';
import { ErrorBoundary } from 'react-error-boundary';

//Set globally BTS-ITALIA Version
import BOOTSTRAP_ITALIA_VERSION from 'bootstrap-italia/dist/version';
import { loadFonts } from 'bootstrap-italia';
import { routes } from "./routes";
import {Errors} from "./pages/Errors";
import {useSelector} from "react-redux";

window.BOOTSTRAP_ITALIA_VERSION = BOOTSTRAP_ITALIA_VERSION;

export function App() {
  const location = useLocation();
  const { error } = useSelector((x) => x.auth);

  useEffect(() => {
    // Importing files depending on env
    if (process.env.REACT_APP_STYLE === 'true') {
      import('./assets/stylesheets/core.scss');
      import('./index.scss');
      loadFonts('https://static.opencityitalia.it/fonts');
    } else {
      import('./index.scss');
    }

  }, []);

  // Scroll to top if path changes
  useLayoutEffect(() => {
    window.scrollTo(0, 0);
  }, [location.pathname]);

  return (
      <div>
      {error ? (
            <ErrorBoundary
                FallbackComponent={Errors}
                fallback={<Errors error={{ name: 'ErrorBoundary' }}></Errors>}>
              <Errors error={{ name: 'ErrorBoundary' }}></Errors>
            </ErrorBoundary>
        ) : (
    <Routes>
      {routes.map((route) => ( <Route key={route.path} path={route.path} element={route.Component} />))}
    </Routes>
  )}
      </div>
  );
}
