import { createSlice } from '@reduxjs/toolkit';

const rootSlice = createSlice({
  name: 'form',
  initialState: {
    application: {}
  },
  reducers: {
    storeApplication: (state, action) => {
      state.application = action.payload;
    }
  }
});

export const formReducer = rootSlice.reducer;

export const { storeApplication } = rootSlice.actions;
